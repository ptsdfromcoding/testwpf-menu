﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Dishes todayMenu;
        Orders todayOrders;
        int Mode;

        public MainWindow()
        {
            Mode = 0;
            todayMenu = new Dishes(Mode);
            todayOrders = new Orders();
            todayOrders.FinalOrders = new List<Order>();

            InitializeComponent();
            
            tMenuView.ItemsSource = todayMenu.dishes; //Data Binding - специально для ListView
            tOrderView.ItemsSource = todayOrders.FinalOrders; //Data Binding - специально для ListView
        }

        private void ChangeMenuClick(object sender, RoutedEventArgs e)
        {
            switch (Mode)
            {
                case 0:
                    {
                        Mode = 1;
                        todayMenu = new Dishes(Mode);
                        tMenuView.ItemsSource = todayMenu.dishes;
                        break;
                    }

                case 1:
                    {
                        Mode = 0;
                        todayMenu = new Dishes(Mode);
                        tMenuView.ItemsSource = todayMenu.dishes;
                        break;
                    }
            }
        }

        //Событие — что-то, что происходит на смену выбранного пункта в ListView
        private void tMenuView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            /*
            dNameView.Text = todayMenu.dishes[tMenuView.SelectedIndex].Name;
            dPriceView.Text = todayMenu.dishes[tMenuView.SelectedIndex].Price.ToString();
            dTimeView.Text = todayMenu.dishes[tMenuView.SelectedIndex].Time.ToString();
            */
        }

        private void AddToOrderClick(object sender, RoutedEventArgs e)
        {
            //при каждом нажатии на кнопку создается отдельный пустой объект
            Order tempOrder = new Order();

            //2 варианта переноса
            //tempOrder.Name = dNameView.Text;                      //из поля
            tempOrder.Name = ((Food)tMenuView.SelectedItem).Name;   //из пункта меню

            //tempOrder.Price = Convert.ToDouble(dPriceView.Text);  //из поля
            tempOrder.Price = ((Food)tMenuView.SelectedItem).Price; //из пункта меню

            //tempOrder.Time = Convert.ToInt32(dTimeView.Text);     //из поля
            tempOrder.Time = ((Food)tMenuView.SelectedItem).Time;   //из пункта меню

            tempOrder.Quantity = Convert.ToInt32(dQuantityView.Text); //из поля

            /*
             * Из поля: строки TextBox в объект;
             * если это число, то его надо конвертировать.
             * Стандартный класс конвертации — Convert.
             * Convert.ToDouble(), Convert.ToInt32 и т.д.
             * В скобках что конвертировать.
             * 
             * Из пункта меню: объект в строке ListView в объект;
             * данные уже нужного типа, конвертировать не надо.
             * Зато надо конвертировать сам объект —
             * ListView не знает, что в нём, ему надо сказать
             * (что это Food, например)
             * Для этого перед выбранным пунктом (объектом)
             * ставится в круглых скобках класс объекта.
             * Это т.н. явное приведение, то есть когда вы
             * напрямую говорите, что это за объект.
             * Обычно C# знает, но в таких случаях — нет.
             */
            //==============================

            //добавить к списку заказов и обновить визуально ListView с заказами
            todayOrders.FinalOrders.Add(tempOrder);
            tOrderView.Items.Refresh();
        }
    }

    public struct Order
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public int Time { get; set; }
        public int Quantity { get; set; }
    }

    public struct Orders
    {
        public List<Order> FinalOrders;
        public int FinalPrice;
        public int FinalTime;
    }
}
