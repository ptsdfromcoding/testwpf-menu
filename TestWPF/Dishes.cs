﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestWPF
{
    public class Dishes
    {
        public List<Food> dishes;

        public Dishes(int mode)
        {
            dishes = new List<Food>();

            if (mode==0)
            {
                Food soup = new Food { Name = "Суп", Price = 100, Time = 600 };
                Food tea = new Food { Name = "Чай", Price = 30, Time = 30 };
                Food steak = new Food { Name = "Стейк", Price = 250, Time = 600 };
                Food rice = new Food { Name = "Рис", Price = 80, Time = 1200 };



                dishes.Add(soup);
                dishes.Add(tea);
                dishes.Add(steak);
                dishes.Add(rice);
            }

            if (mode==1)
            {
                Food tea = new Food { Name = "Чай", Price = 30, Time = 30 };
                Food rice = new Food { Name = "Рис", Price = 80, Time = 1200 };

                dishes.Add(tea);
                dishes.Add(rice);
            }
        }
    }
}
