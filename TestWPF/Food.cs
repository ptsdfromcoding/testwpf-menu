﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestWPF
{
    public class Food
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public int Time { get; set; }

        /*
        public override string ToString()
        {
            return this.Name + ", " + this.Price + " руб.";
        }
        */
    }
}